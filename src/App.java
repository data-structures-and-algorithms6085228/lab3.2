public class App {

    public static int search(int[] nums, int target) {
        int low = 0, high = nums.length - 1;

        while (low <= high) {
            int mid = (low + high) / 2;

            if (nums[mid] == target) {
                return mid;
            }

            if (nums[low] <= nums[mid]) {
                if (nums[low] <= target && target < nums[mid]) {
                    high = mid - 1;
                } else {
                    low = mid + 1;
                }
            } else {
                if (nums[mid] < target && target <= nums[high]) {
                    low = mid + 1;
                } else {
                    high = mid - 1;
                }
            }
        }

        return -1;
    }
    public static void main(String[] args) throws Exception {
        int nums1[] = {4,5,6,7,0,1,2};
        int target1 = 0;
        int s1 = search(nums1, target1);
        System.out.println(s1);

        int nums2[] = {4,5,6,7,0,1,2};
        int target2 = 3;
        int s2 = search(nums2, target2);
        System.out.println(s2);

        int nums3[] = {1};
        int target3 = 0;
        int s3 = search(nums3, target3);
        System.out.println(s3);
    }
}
